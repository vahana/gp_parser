%==========================================================================
% input values: what quantities to plot and what speed to plot at 
%==========================================================================

icol            = 3;        % 1,2: c,w tilt; 3,4: c,w RPMs; 5: th0; 6: elevator

%==========================================================================
% 5 m/s: check pitching moment
%==========================================================================

V               = 5;
irow            = [5];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 15 m/s: check vertical force
%==========================================================================

V               = 15;
irow            = [3];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 25 m/s: check pitching moment
%==========================================================================

V               = 25;
irow            = [5];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 50 m/s: check longitudinal force
%==========================================================================

V               = 50;
irow            = [1];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

disp('completed perturbation studies for canard rotor RPMs')