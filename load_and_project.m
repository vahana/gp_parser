%==========================================================================
% Matlab function to load data from files and create detailed x,y
% combinations for perturbing control input "icol" and studying 
% prediction of loads with indices given by array "irow" (vector)
% purpose is to generate raw data for comparing charm (linearized)
% and GP model predictions of loads sensitivities to control inputs
% for different perturbation sizes
%==========================================================================

function [x,yc,y] = load_and_project(V, charm_path, gp_path, irow, icol)

%==========================================================================
% load trim state (charm results)
%==========================================================================

[new_der,X0]    = load_charm(V, charm_path);

%==========================================================================
%for the trained model
%==========================================================================

%==========================================================================
% with the GP model and the trim vector, calculate values of GP model 
% at several points
%==========================================================================

[x,xp,y,lb,ub]  = gp_data_points(V, X0, irow, icol, gp_path);

%==========================================================================
% perturb charm model with limits identified from GP fitted data
%==========================================================================

[~,~,yc]        = charm_points(new_der,irow,icol,lb,ub);

%==========================================================================
% save results to disk
%==========================================================================

%==========================================================================
% x axis values: perturbations
%==========================================================================

common  = strcat('save -ascii',{' '},'detailed/','input_',num2str(icol),'_V_',num2str(V),'_');
expr    = strcat(common,'xaxis.dat xp');
eval(expr{1});

%==========================================================================
% y axis values: delta loads wrt trim (linearized model)
%==========================================================================

expr    = strcat(common,'ycharm.dat yc');
eval(expr{1});

%==========================================================================
% y axis values: delta loads wrt trim (GP model)
%==========================================================================

expr    = strcat(common,'ygp.dat y');
eval(expr{1});

%==========================================================================
% remember which columns were perturbed
%==========================================================================

expr    = strcat(common,'rowids.dat irow');
eval(expr{1})