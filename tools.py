import os, sys, numpy, copy

#======================================================================================
# function to identify airspeed in m/s from a filename
#======================================================================================

def find_speed(filename, suf):
	f 		= filename.rstrip(suf).split('_')[-1]
	return int(f)

#======================================================================================
# function to parse a folder with output files and extract airspeeds 
#======================================================================================

def parse_directory(paths, suffix_strip):

	speed_lists			= []

	for path,suf in zip(paths,suffix_strip):
		this_list 		= []
		for root, dirnames, filenames in os.walk(path):
			for filename in filenames:

				if(filename.endswith(suf)):
					this_list.append(find_speed(filename,suf))
		speed_lists.append(sorted(this_list))

	intersect_list 		= []

#======================================================================================
#loop over all speeds in reference list
#======================================================================================

	for speed in speed_lists[0]:
		found_in_all	= True 

#======================================================================================
#check for this speed in all lists
#======================================================================================

		for l in speed_lists:
			Found 		= False 
			for s in l:
				if (s == speed):
					Found 	= True 
					break 
			if not Found:
				found_in_all 	= False 
		
		if found_in_all:	
			intersect_list.append(speed)


	return intersect_list

#======================================================================================
# function to load a single data file
#======================================================================================

def load_dat(fname):
	
	print('loading file',fname)
	d 		= numpy.loadtxt(fname)
	return d 

#======================================================================================
# function to load all data into nested dictionaries from data paths 
#======================================================================================

def load_data(paths, suffixes, speeds): 

	data 		= {}
	data2		= {}
	for path,suf in zip(paths,suffixes):
		key 	= path.rstrip('/')
		sub		= {}
		found 	= False
		for root, dirnames, filenames in os.walk(path):
			for filename in filenames:
				if(filename.endswith(suf)):
					V 		= find_speed(filename,suf)
					if V in speeds:

						ids 			= speeds.index(V) 		# 3rd dimension index
						fname 			= path+filename 
						this_d  		= load_dat(fname)

						if(not found): 							# size not found
							nr 		= numpy.shape(this_d)
							nc 		= nr[1]
							nr 		= nr[0]

							ald  	= numpy.zeros((nr,nc,len(speeds))) 		# remember in 3d array
							found   = True
							if(ids == 0): 
								print('initialized array of size',nr,' x ', nc,' x ',len(speeds))
						ald[:,:,ids]	= copy.copy(this_d)

					sub[V]  = this_d 	

		data[key] 	= sub
		data2[key] 	= ald

	return data,data2

#======================================================================================
# function to map loads to row indices
#======================================================================================

def load_index_map(loads):

	row_ids 		= []
	strings 		= []
	units	 		= []
	for r in loads:
		if(r == 'X'):
			row_ids.append(0)
			units.append('N')
			strings.append('Longitudinal force')
		if(r == 'Y'):
			row_ids.append(1)
			units.append('N')
			strings.append('Lateral force')
		if(r == 'Z'):
			row_ids.append(2)
			units.append('N')
			strings.append('Vertical force')
		if(r == 'L'):
			row_ids.append(3)
			units.append('N-m')
			strings.append('Rolling moment')
		if(r == 'M'):
			row_ids.append(4)
			units.append('N-m')
			strings.append('Pitching moment')
		if(r == 'N'):
			row_ids.append(5)
			units.append('N-m')
			strings.append('Yawing moment')
		if(r.startswith('T')):
			rid 	= int(r.lstrip('T'))
			row_ids.append(5+rid)
			units.append('N')
			strings.append('Rotor thrust')
		if(r.startswith('Q')):
			rid 	= int(r.lstrip('Q'))
			row_ids.append(13+rid)
			units.append('N-m')
			strings.append('Rotor torque')

	return row_ids,strings,units

#======================================================================================
# function to map perturbation to column indices
#======================================================================================

def pert_index_map(pert):

	col_ids 		= []
	strings 		= []
	units 			= []
	for c in pert:
		if(c == 'tauc'): 				# canard tilt
			col_ids.append(0)
			strings.append('Canard tilt angle $\\tau_c$')
			units.append('rad')
		if(c == 'tauw'): 				# wing   tilt
			col_ids.append(1)
			strings.append('Wing tilt angle $\\tau_w$')
			units.append('rad')
		if(c == 'omsqc'): 				# RPM squared for canard rotors
			col_ids.append(2)			
			strings.append('Canard RPMs $\\Omega^2_{1-4}$')
			units.append('(rad/s)$^2$')
		if(c == 'omsqw'): 				# RPM squared for wing rotors
			col_ids.append(3)			
			strings.append('Wing RPMs $\\Omega^2_{5-8}$')
			units.append('(rad/s)$^2$')
		if(c == 'phi'): 				# collective pitch for all rotors
			col_ids.append(4)
			strings.append('Collective $\\phi_{1-8}$')
			units.append('rad')
		if(c == 'ele'): 				# ??
			col_ids.append(5)
			strings.append('Elevator $\\delta_{1,2}$')
			units.append('rad')

	return col_ids, strings,units