%==========================================================================
% matlab function to get linear model predicted data at several points
% irow = load component index, can be an array
% icol = control input index
%
% output data is in matrix form
% each column contains y-axis values of the linear model prediction
% successive columns correspond to the load components indicated by irow
%==========================================================================

function [delta,dp,yc] = charm_points(Jmat,irow,icol,lb,ub)

[delta,dp]      = xaxis_range(icol,lb,ub);
yc              = zeros(length(delta),length(irow));

for j=1:length(irow)
    yc(:,j)     = Jmat(irow(j),icol)*delta;
end

return