clear all
clear
close all
clc

%==========================================================================
% define aero2 location 
%==========================================================================

home        = getenv('HOME');
aero2       = strcat(home,'/codes/aero2/');

%==========================================================================
% install GPML toolbox
%==========================================================================

% install_GPML_shortcut(aero2);

%==========================================================================
% location of mat files containing required information from charm
%==========================================================================

path        = '';
p1          = strcat(path,'charm/'); airspeeds   = [0,5,15,20,25,30,35,40,45,50];
p3          = strcat(path,'trim_vector/');

%==========================================================================
% load data
%==========================================================================

for i=1:length(airspeeds)
    V       = airspeeds(i);
    fname   = strcat('LINEAR_AERO_U_',num2str(V),'.mat');
    file    = strcat(p1,fname);
    expr    = strcat('load ',{' '},file);
    eval(expr{1});
    LM      = DATABASE.LM;          X0      = DATABASE.X0;
    derivs  = [LM.DFDX; LM.DMDX; LM.DVDX; LM.DTDX];
    
    new_der = zeros(22,6);

%==========================================================================
% combine individual derivatives so that we get Jacobians wrt parameters in 
% the GP model 
%==========================================================================

    new_der(:,1)  = derivs(:,25);               % canard tilt angle
    new_der(:,2)  = derivs(:,26);               % wing   tilt angle
    new_der(:,3)  = sum(derivs(:,13:16),2);     % rpm squared for all 4 canard rotors
    new_der(:,4)  = sum(derivs(:,17:20),2);     % rpm squared for all 4 wing   rotors
    new_der(:,5)  = sum(derivs(:,27:34),2);     % collective pitch of all 8 rotors
    new_der(:,6)  = sum(derivs(:,21:22),2);     % canard elevator deflection angle
    
%==========================================================================
% save the results to disk (including trim vector)
%==========================================================================

    expr    = strcat('save -ascii',{' '},p1,'derivs_U_',num2str(V),'.dat new_der');
    eval(expr{1});
    expr    = strcat('save -ascii',{' '},p3,'X0_U_',num2str(V),'.dat X0');
    eval(expr{1});
    disp(expr)
end

%==========================================================================
%for the second path: trained model
%==========================================================================

path        = '';
p2          = strcat(path,'GP/');    airspeeds   = [0 5 15:5:50];
p3          = strcat(path,'trim_vector/');

%==========================================================================
% loop over airspeeds and load the fitted GP model
%==========================================================================

for i=1:length(airspeeds)
    V       = airspeeds(i);
    fname   = strcat('AERODB_Vahana_AlphaV2_U_',num2str(V),'_TRAINED.mat');
    file    = strcat(p2,fname);
    expr    = strcat('load ',{' '},file);
    eval(expr{1});

%==========================================================================
% load the trim vector from the dat file
%==========================================================================
    
    fname   = strcat(p3,'X0_U_',num2str(V),'.dat'');');
    expr    = strcat('X0 = load(''',fname);
    eval(expr);

%==========================================================================
% with the GP model and the trim vector, get the long. control derivatives
%==========================================================================

    Jmat    = long_derivs(GPFIT, X0);

%==========================================================================
% save results to disk
%==========================================================================

    expr    = strcat('save -ascii',{' '},p2,'derivs_U_',num2str(V),'.dat Jmat');
    eval(expr{1});
    disp(expr)
end

%==========================================================================
% 
%==========================================================================
