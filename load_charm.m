function [new_der, X0] = load_charm(V, path)

fname       = strcat(path,'LINEAR_AERO_U_',num2str(V),'.mat');
expr        = strcat('load ',{' '},fname);
eval(expr{1});
LM          = DATABASE.LM;          X0      = DATABASE.X0;
derivs      = [LM.DFDX; LM.DMDX; LM.DVDX; LM.DTDX];    
new_der     = zeros(22,6);

%==========================================================================
% combine individual derivatives so that we get Jacobians wrt parameters in 
% the GP model 
%==========================================================================

new_der(:,1)  = derivs(:,25);               % canard tilt angle
new_der(:,2)  = derivs(:,26);               % wing   tilt angle
new_der(:,3)  = sum(derivs(:,13:16),2);     % rpm squared for all 4 canard rotors
new_der(:,4)  = sum(derivs(:,17:20),2);     % rpm squared for all 4 wing   rotors
new_der(:,5)  = sum(derivs(:,27:34),2);     % collective pitch of all 8 rotors
new_der(:,6)  = sum(derivs(:,21:22),2);     % elevator deflection on canard
    

return