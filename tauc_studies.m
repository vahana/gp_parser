%==========================================================================
% input values: what quantities to plot and what speed to plot at 
%==========================================================================

icol            = 1;        % 1,2: c,w tilt; 3,4: c,w RPMs; 5: th0; 6: elevator

%==========================================================================
% 15 m/s: check longitudinal body force
%==========================================================================

V               = 15;
irow            = [1];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 50 m/s: check body X force, torques of rotors 1,2,3,4
%==========================================================================

V               = 50;
irow            = [1 15 16 17 18];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

disp('completed perturbation studies for canard tilt')