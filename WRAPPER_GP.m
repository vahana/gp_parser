function [loads,XREDUCED] = WRAPPER_GP (X, GPFIT)
%
% Function: WRAPPER_GP
%
% Author: Giovanni Droandi
%   Mail: giovanni.droandi@airbus-sv.com
% modified by Ananth Sridharan for unit consistency: deg to rad
%
% This function takes the complete state vector and the surrogate model given for a specific flight 
% speed and return total aerodynamic forces and moments (reduced at the MRC) in body axis.
% 
% Complete state vector X (34x1):
% X(1)     = x_cg                evolving x-coordinate of CG, body-frame [m]
% X(2)     = y_cg                evolving y-coordinate of CG, body-frame [m]
% X(3)     = z_cg                evolving z-coordinate of CG, body-frame [m]
% X(4)     = phi                 euler orientation of body-axis [rad]
% X(5)     = theta               euler orientation of body-axis [rad]
% X(6)     = psi                 euler orientation of body-axis [rad]
% X(7)     = u                   x-velocity component, body-frame [m/s]
% X(8)     = v                   y-velocity component, body-frame [m/s]
% X(9)     = w                   z-velocity component, body-frame [m/s]
% X(10)    = p                   roll rate, body-frame [rad/s]
% X(11)    = q                   pitch rate, body-frame [rad/s]
% X(12)    = r                   yaw rate, body-frame [rad/s]
% X(13:20) = w^2_(1-8)           fans angular velocity squared [(rad/s)^2]
% X(21:22) = delta_(c,1-2)       canard control surface deflection, left and right [rad]
% X(23:24) = delta_(w,3-4)       wing control surface deflection, left and right [rad]
% X(25)    = theta_(c)           canard tilt angle [rad]
% X(26)    = theta_(w)           wing tilt angle [rad]
% X(27:34) = collective_(1-8)    fans collective pitch [rad]
%

% ..Getting the reduced state vector (re-shaped for GP) from the complete state vector X

% ..Angle of attack [deg]
XREDUCED(1) = atan2d(X(9),X(7));

r2d         = 180.0/pi;

% Canard tilt angle [deg]
XREDUCED(2) = X(25)*r2d;

% Wing tilt angle [deg]
XREDUCED(3) = X(26)*r2d;

% Elevator deflection angle [deg]
XREDUCED(4) = mean(X(21:22))*r2d;

% Aileron deflection angle [deg]
XREDUCED(5) = mean(X(23:24))*r2d;

% Canard fan rotational speed [(rad/s)^2]
XREDUCED(6) = mean(X(13:16));

% Wing fan rotational speed [(rad/s)^2]
XREDUCED(7) = mean(X(17:20));

% Canard fan collective [deg]
XREDUCED(8) = mean(X(27:30))*r2d;

% Wing fan collective [deg]
XREDUCED(9) = mean(X(31:34))*r2d;

% ..Computing loads with Gaussian Process (GP) toolbox
LOADS = COMPUTE_GP (XREDUCED, GPFIT);

% ..Outputing body loads, rotor thrusts (positive up) and torque
loads = [LOADS.TOTAL(1:6) -LOADS.FAN.T LOADS.FAN.Q]';

end