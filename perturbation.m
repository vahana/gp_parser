%==========================================================================
% This function takes the trim vector X0, the pertubation numerical value 
% "delta" and the fitted parameter index "id" for the GP model and returns 
% the perturbed vector "Xout" 
%==========================================================================

function Xout   = perturbation(X0, id, delta)

%==========================================================================
% initialize trim vector to X0
%==========================================================================

Xout        = X0; 

if(id == 1)                         % canard tilt
    Xout(25) = Xout(25) + delta;
elseif(id == 2)                     % wing tilt
    Xout(26) = Xout(26) + delta;    
elseif(id == 3)                     % RPM squared for canard rotors
    Xout(13:16) = Xout(13:16) + delta;
elseif(id == 4)                     % RPM squared for wing   rotors 
    Xout(17:20) = Xout(17:20) + delta;    
elseif(id == 5)                     % Collective  for all 8  rotors 
    Xout(27:34) = Xout(27:34) + delta;
elseif(id == 6)                     % canard elevator deflections
    Xout(21:22) = Xout(21:22) + delta;    
else
    ERROR('CRITICAL ERROR: CAN ONLY PERTURB UP TO INDEX 6')
end 

%==========================================================================
% end of operations
%==========================================================================

return