

from tools import parse_directory, load_data, load_index_map, pert_index_map
import numpy
import matplotlib.pyplot as plt
from matplotlib import rc 
from matplotlib.backends.backend_pdf import PdfPages

rc('text',usetex=True)
font = {'family' : 'serif',
        'weight' : 'bold',
        'size'   : 18}

rc('font', **font)
#======================================================================================
# python script to check longitudinal stability derivatives from original charm and 
# GP trained model 
#======================================================================================

#======================================================================================
# define paths where data exists
#======================================================================================

paths       	= ['charm/','GP/']

#======================================================================================
# define file name prefixes and suffixes to strip out
#======================================================================================

suffix_strip 	= ['.dat','.dat']#'_TRAINED.mat']

#======================================================================================
# parse through all directories and identify common airspeeds 
#======================================================================================

speeds 			= parse_directory(paths, suffix_strip)
	
#======================================================================================
# load data files
#======================================================================================
		
data,d2			= load_data(paths,suffix_strip,speeds)

#======================================================================================
# define first element (load components) of stability derivatives
#======================================================================================

loads 			= ['X','Z', 'M','T1','T2','T3','T4','T5','T6','T7','T8','Q1','Q2','Q3','Q4','Q5','Q6','Q7','Q8']
id_loads, y, ur = load_index_map(loads)

#======================================================================================
# define second component (pertubrations) of stability derivatives
#======================================================================================

pert 			= ['tauc','tauw','omsqc','omsqw', 'phi','ele']
id_pert, x, uc  = pert_index_map(pert)

#======================================================================================
# create plots of natural dynamics stability derivatives: 
# Xu, Xw, Xq, Zu, Zw, Zq, Mu, Mw, Mq
#======================================================================================

styles 			= ['-','o']
p 				= {'weight':'normal','size':12}
active 			= False 
with PdfPages('big_picture.pdf') as pdf:

	for col,SC,stx,cu in zip(id_pert,pert,x,uc):
		for row,FC,sty,ru in zip(id_loads,loads,y,ur): 	

#======================================================================================
# filter out indirect sensitivities
#======================================================================================

			if(FC.startswith('T') or FC.startswith('Q')):
				rid 		= int(FC[-1])
				if((rid < 5 and SC.endswith('w')) or (rid > 4 and SC.endswith('c'))):
					save_plot = False
					continue

#======================================================================================
# for rotor thrust/torque, plot only direct sensitivities
#======================================================================================

			plt.figure(1) 					# plot of Xu: array index 0,0
			for key,value in d2.items(): 			# different types of data: charm or GP
	#			print(key,value[row,col,1])
				if(key == 'charm'):
					style   = ''
					marker  = 's' if active else 'o'
					fill 	= 'top' if active else 'bottom'
				else:
					marker  = ''
					style 	= '--' if active else '-'
					fill 	= 'none'
				color   = 'C1' if active else 'C0'


				label 		= key.ljust(5)
				if(FC.startswith('T') or FC.startswith('Q')):
					label 	= 'Rotor '+str(rid) + ': ' + key.ljust(5)
				else:
					label 	= key
				plt.plot(speeds,value[row,col,:],linestyle=style,marker=marker,fillstyle=fill,label=label,linewidth=2.0,color=color)
				plt.xlabel('Airspeed (m/s)')
				plt.ylabel(ru + '/' + cu)
				plt.title(sty+' sensit. to ' + stx)
			plt.legend(prop=p)
			plt.tight_layout(pad=1.2)
			plt.grid(linestyle='--')
			fname 		= FC+'_'+SC+'.png'

			if(FC.startswith('T') or FC.startswith('Q')):
				rid 		= int(FC[-1])
				if rid % 2 == 0:
					save_plot = True
				else:
					save_plot = False
					print('not yet saving plot',FC,SC) 

			else:
				save_plot 	  = True 

			if save_plot:
				print('saving file',fname)
				pdf.savefig()
				plt.close()

				active 		  = False 
	#			print('reset colors switch')
			else:
				active 	      = True
	#			print('setting colors to C1')