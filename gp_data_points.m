%==========================================================================
% matlab function to get GP model predicted data at several points
% irow = load component index
% icol = control input index
%==========================================================================

function [delta,dp,mat,lb,ub] = gp_data_points(V,X0,irow,icol,path)

%==========================================================================
% load the fitted GP model
%==========================================================================

fname       = strcat(path,'AERODB_Vahana_AlphaV2_U_',num2str(V),'_TRAINED.mat');
expr        = strcat('load ',{' '},fname);
eval(expr{1});

%==========================================================================
% get output for trim point X0
%==========================================================================

[f0,XRef]   = WRAPPER_GP(X0, GPFIT);           % baseline loads 

%==========================================================================
% Calculate deltas for the GPFIT model
%==========================================================================

[lb,ub]     = get_bounds(GPFIT, XRef);

%==========================================================================
% identify delta ranges
%==========================================================================

[delta,dp]  = xaxis_range(icol,lb,ub);
% delta
% dp
% blah
%==========================================================================
% get perturbed trim vector for each of the six perturbations
%==========================================================================

mat         = zeros(length(delta),length(irow));

for j=1:length(delta)
    X       = perturbation(X0, icol, delta(j));
    fp      = WRAPPER_GP(X, GPFIT);
    for i=1:length(irow)
        mat(j,i) = fp(irow(i)) - f0(irow(i));
    end
end 

%==========================================================================
% end of operations
%==========================================================================

return