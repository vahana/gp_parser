%==========================================================================
% for RPM squared: range is +/- 15%
% for all other inputs: angles range from +/- 30 deg
%==========================================================================

function [delta,dp] = xaxis_range(icol, minvals, maxvals)

if(icol == 3 || icol == 4)
%     delta      = -15000:2500:15000;
    delta      = linspace(minvals(icol),maxvals(icol),11);
    dp         = delta;

else
    dp         = linspace(minvals(icol),maxvals(icol),11);
%     dp         = -30:5:30;
    delta      = dp*pi/180;
end 

return 