%==========================================================================
% input values: what quantities to plot and what speed to plot at 
%==========================================================================

icol            = 5;        % 1,2: c,w tilt; 3,4: c,w RPMs; 5: th0; 6: elevator

%==========================================================================
% 0 m/s: check longitudinal force sensitivity
%==========================================================================

V               = 0;
irow            = [1 5 13 15:22];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 15 m/s: check pitching moment, thrust and torque of all rotors
%==========================================================================

V               = 15;
irow            = [5 7:22];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 20 m/s: check pitching moment
%==========================================================================

V               = 20;
irow            = [5];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 50 m/s: check torque of rotors 5 - 8
%==========================================================================

V               = 50;
irow            = [19 20 21 22];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

disp('completed perturbation studies for collective collective of rotors')