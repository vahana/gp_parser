function [LOADS] = COMPUTE_GP (X, GPFIT)
%
% Function: COMPUTE_GP
%
% Author: Giovanni Droandi
%   Mail: giovanni.droandi@airbus-sv.com
%
% Given the air speed, the function takes the corresponding surrogate model and the actual state 
% vector as inputs and returns forces and moments at the MRC of the vehicle. Forces and moments are 
% computed by using the Gaussian Process for Machine Learning GPML toolbox (version 4.0 released 
% October 19th 2016) for Matlab. The GPML toolbox is compatible with Matlab 7.x or later versions. 
% It has been developed by written by Carl Edward Rasmussen and Chris Williams and documentation can 
% be found here: http://www.gaussianprocess.org/
%
% Inputs:
% 
% 1. GPFIT : Structure containing the surrogate model. Surrogate model output training values 
%            (training values, mean and standard deviation) are provided as follows:
%              1. Total aerodynamic loads (positions from 1 to 6) : forces [N] and moments [Nm] of
%                 the vehicle reduced at the MRC and given in body axis
%              2. Aerodynamic torque (positions from 7 to 14) : torques [Nm] for each fan, 
%                 from n.1 to n.8
%              3. Aerodynamic thrust (positions from 15 to 22) : thrusts [N] for each fan, 
%                 from n.1 to n.8
% 2. X     : Actual state vector containing variables and parameters in the following order:
%              1. Vehicle pitch angle, alpha [deg]
%              2. Canard tilt angle, tau_c [deg]
%              3. Wing tilt angle, tau_w [deg]
%              4. Elevator deflection angle, delta_1,2 [deg]
%              5. Aileron deflection angle, delta_3,4 [deg]
%              6. Canard fan rotational speed squared, omega_1-4^2 [(rad/s)^2]
%              7. Wing fan rotational speed squared, omega_5-8^2 [(rad/s)^2]
%              8. Canard fan collective, theta_1-4 [deg]
%              9. Wing fan collective, theta_5-8 [deg]
%
% Outputs:
%
% 1. LOADS : Structure containing output loads predicted by using the GP. The structure contains the 
%            following fields:
%              .TOTAL : total aerodynamic loads (Fx, Fy, Fz, Mx, My, Mz), forces [N] and moments 
%                       [Nm] of the vehicle reduced at the MRC and given in body axis
%              .FAN.T : fans thrusts (T1, T2, ... T8), each fan thrust [N] is given in the local 
%                       shaft reference frame (along z_shaft, positive downward)
%              .FAN.Q : fans torques (Q1, Q2, ... Q8), each fan torque [Nm] is given in the local 
%                       shaft reference frame (along z_shaft, positive downward)
%

% ..Computing GP inputs form actual XGP (full state vector)
INPUTS = (X(GPFIT.LABEL==1) - GPFIT.XMEAN) ./ GPFIT.XSTD;

% ..Defining arrays
OUTPUTS = zeros(length(GPFIT.OUTPUTS(1,:)),1);

% ..Running the current GP analysis
for I = 1:length(GPFIT.OUTPUTS(1,:))
    OUTPUTS(I) = gp(GPFIT.HYP(I), ...
                    GPFIT.IM, ...
                    GPFIT.MEANFUNC, ...
                    GPFIT.COVFUNC, ...
                    GPFIT.LIKFUNC, ...
                    GPFIT.XTRAIN, ...
                    GPFIT.YTRAIN(:,I), ...
                    INPUTS);
end

% ..Extracting thrust, torque and power for each fan from GP outputs
LOADS = struct;

% ..Computing total Fx, Fz and My in body frame (moment reduced at the MRC)
LOADS.TOTAL = zeros(1,6);
LOADS.TOTAL(1) = OUTPUTS(1) * GPFIT.YSTD(1) + GPFIT.YMEAN(1);
LOADS.TOTAL(3) = OUTPUTS(3) * GPFIT.YSTD(3) + GPFIT.YMEAN(3);
LOADS.TOTAL(5) = OUTPUTS(5) * GPFIT.YSTD(5) + GPFIT.YMEAN(5);

for I = 1:8
    % Fan thrust [N]
    LOADS.FAN.T(I) = OUTPUTS(14+I) * GPFIT.YSTD(14+I) + GPFIT.YMEAN(14+I);
    % Fan torque [Nm]
    LOADS.FAN.Q(I) = OUTPUTS(6+I) * GPFIT.YSTD(6+I) + GPFIT.YMEAN(6+I);
end

end