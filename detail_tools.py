#======================================================================================
# tools used to make detailed plots to compare gp model outputs 
# walk through directory and identify cases
# cases identified by input index "input" and airspeed "V" in filename
#======================================================================================

import os, numpy
from copy import copy
def find_control_ids(path):
    input_ids       = []
    for root, dirnames, filenames in os.walk(path):
        for filename in filenames:

#======================================================================================
# use xaxis file to identify cases
#======================================================================================

            if filename.rstrip('.dat').endswith('xaxis'):
                f    = filename.lstrip('input_').rstrip('_xaxis.dat').split('_')
                V    = int(f[-1])
                idc  = int(f[0])
#                print(V,idc)

                if idc not in input_ids:
                    input_ids.append(int(idc))
    return input_ids


#======================================================================================
# get file names for a given control id
#======================================================================================

def find_speeds(path, idc):

    speeds       = []

    for root, dirnames, filenames in os.walk(path):
        for filename in filenames:

#======================================================================================
# use xaxis file to identify cases
#======================================================================================

            if filename.rstrip('.dat').endswith('xaxis'):
                f    = filename.lstrip('input_').rstrip('_xaxis.dat').split('_')
                V    = int(f[-1])
                cid  = int(f[0])

                if(idc == cid):
                    speeds.append(V)

    return speeds

#======================================================================================
# get load component string from row #
#======================================================================================

def interpret_rows(rows):
    strings         = []
    units           = []

    for r in rows:
        if(r == 1):
            units.append('$\\Delta$F$_X$ (N)')
            strings.append('Body F$_{_X}$')
        if(r == 2):
            units.append('$\\Delta$F$_Y$ (N)')
            strings.append('Body Y force')
        if(r == 3):
            units.append('$\\Delta$F$_Z$ (N)')
            strings.append('Body Z force')
        if(r == 4):
            units.append('$\\Delta$M$_X$ (N-m)')
            strings.append('Rolling moment')
        if(r == 5):
            units.append('$\\Delta$M$_Y$ (N-m)')
            strings.append('Body M$_{Y}$')
        if(r == 6):
            units.append('$\\Delta$M$_Z$ (N-m)')
            strings.append('Yawing moment')
        if(r > 6 and r <= 14):
            units.append('$\\Delta$Thrust (N)')
            strings.append('Rotor '+str(r-6)+' thrust')
        if(r > 14 and r <= 22):
            units.append('$\\Delta$Torque (N-m)')
            strings.append('Rotor '+str(r-14) +' torque')
        if(r < 1 or r > 22):
            print('row id entered is ',r)
            quit('unknown: choose row id from 1 to 22')
    return strings, units

#======================================================================================
# function to get units and interpret control input from an index
#======================================================================================

def interpret_input(c):

    string  = [] 
    unit    = []
    if(c == 1):                # canard tilt
        string = 'Canard tilt angle $\\tau_c$'
        unit   = ' (deg)'
        symbol = '$\\tau_c$'
    if(c == 2):                # wing   tilt
        string = 'Wing tilt angle $\\tau_w$'
        unit   = ' (deg)'
        symbol = '$\\tau_w$'

    if(c == 3):               # RPM squared for canard rotors
        string = 'Canard rotor RPMs $\\Omega^2_{1-4}$'
        unit   = ' (rad/s)$^2$'
        symbol = '$\\Omega^2_{1-4}$'
    
    if(c == 4):               # RPM squared for wing rotors
        string = 'Wing rotor RPMs $\\Omega^2_{5-8}$'
        unit   = ' (rad/s)$^2$'
        symbol = '$\\Omega^2_{5-8}$'

    if(c == 5):                 # collective pitch for all rotors
        string = 'Collective $\\phi_{1-8}$'
        unit   = ' (deg)'
        symbol = '$\\phi_{1-8}$'

    if(c == 6):                 # collective pitch for all rotors
        string = 'Elevator $\\delta_{1,2}$'
        unit   = ' (deg)'
        symbol = '$\\delta_{1,2}$'
    return unit,string,symbol

#======================================================================================
# read data from a folder and load it into nested dictionaries
#======================================================================================

def read_data(path, input_ids, speeds):

    d                   = {}

#======================================================================================
# loop over control input indices, and airspeeds for each control input within
#======================================================================================

    print('input IDs are',input_ids)
    for ic,i in enumerate(input_ids):
        print('input number ',ic,'is ',i)
        for V in speeds[ic]:

            this        = {}

#======================================================================================
# build file name strings and load/read
#======================================================================================
            
            common      = path + 'input_'+str(i)+'_V_'+str(V)+'_'

            x           = numpy.loadtxt(common+'xaxis.dat')
            yc          = numpy.loadtxt(common+'ycharm.dat')             
            ygp         = numpy.loadtxt(common+'ygp.dat')

            try:
                nrows,ncols = numpy.shape(yc)
            except:
                nrows   = numpy.shape(yc)[0]
                ncols   = 1
                yc1     = copy(yc)
                ygp1    = copy(ygp)
                yc      = numpy.zeros((nrows,ncols))
                ygp     = numpy.zeros((nrows,ncols))
                for ir in range(nrows):
                    yc[ir,0]     = yc1[ir]
                    ygp[ir,0]    = ygp1[ir]

            rows        = []
            with open(common+'rowids.dat','r') as f:
                line    = f.readline().rstrip('\n').lstrip(' ').split(' ')
            for row in line:
                if row != '':
                    rows.append(int(eval(row)))
            strings     = []

            for row in rows:
                sy,uy   = interpret_rows(rows)

            xlabel, xs,xsym  = interpret_input(i)
            this        = {'x': x, 'yc': yc, 'ygp':ygp, 'rows': rows, 'V': V, 'input': i, 
                           'yqty': sy, 'yunits': uy, 'xlabel': xsym + xlabel, 'xqty':xs,
                           'xsymbol': xsym}

            d[common]   = this
                    
    return d