%==========================================================================
% input values: what quantities to plot and what speed to plot at 
%==========================================================================

icol            = 4;        % 1,2: c,w tilt; 3,4: c,w RPMs; 5: th0; 6: elevator

%==========================================================================
% 15 m/s: check pitching moment, thrust of rotors 5,6,7,8
%==========================================================================

V               = 15;
irow            = [5 11 12 13 14];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 45 m/s: check pitching moment
%==========================================================================

V               = 45;
irow            = [5];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 50 m/s: check pitching moment
%==========================================================================

V               = 50;
irow            = [5];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

disp('completed perturbation studies for wing rotor RPMs')