%==========================================================================
% find min and max limits of deltas for GP model fits
%==========================================================================

function [lb, ub] = get_bounds(GPFIT, XRef)

minvals     = min(GPFIT.VARIABLES);
maxvals     = max(GPFIT.VARIABLES);

% minvals([1 2 3 6 7])
% maxvals([1 2 3 6 7])
%==========================================================================
% pad GPFIT.VARIABLES (min/max limits) with zeros corresponding to LABEL
% so final output has same size as XRef (input vector to GP trained model)
%==========================================================================

indices     = [2 3 6 7 8 4];    % order of output : 1,2: c,w tilt; 3,4: c,w RPMs; 5: th0; 6: elevator

icount      = 0;
lb          = zeros(length(XRef),1);
ub          = zeros(length(XRef),1);
labels      = GPFIT.LABEL;
for i=1:length(labels)
    if(labels(i) == 0)
        lb(i)  = 0;
        ub(i)  = 0;
    else
        icount = icount + 1;
        lb(i)  = minvals(icount);
        ub(i)  = maxvals(icount);
    end 
end

%==========================================================================
% take delta wrt trim value for GP model input
%==========================================================================

lb          = lb(indices) - XRef(indices)';
ub          = ub(indices) - XRef(indices)';

% lb([1 2 5 6])
% temp    = XRef(indices); temp([1 2 5 6])
% ub([1 2 5 6])

% lb([1 2 5 6]) = lb([1 2 5 6])*pi/180;
% ub([1 2 5 6]) = ub([1 2 5 6])*pi/180;
end