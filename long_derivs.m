%==========================================================================
% This function takes the trim vector X0, the pertubation numerical value 
% "delta" and the fitted parameter index "id" for the GP model and returns 
% the perturbed vector "Xout" 
%==========================================================================

function Jmat = long_derivs(GPFIT, X0)

%==========================================================================
% get output for original X0
%==========================================================================

[f0,XRef]   = WRAPPER_GP(X0, GPFIT);           % baseline loads 

[lb, ub] = get_bounds(GPFIT, XRef);

%==========================================================================
% get perturbed trim vector for each of the six perturbations
%==========================================================================

drpmsq  = 5000;
dang    = 0.05;
deltas  = [dang dang drpmsq drpmsq dang dang];      % 0.05 rad for angles, ~50 rad/s for RPM changes
%for forward difference
deltas  = min(deltas, ub);                          % check vs. min limits

Jmat        = zeros(length(f0),length(deltas));

for i = 1:length(deltas)

    %forward difference
	X           = perturbation(X0, i, deltas(i));
    fp          = WRAPPER_GP(X, GPFIT);
    
%required for central and backwards differences
% 	X           = perturbation(X0, i,-deltas(i));
%     fm          = WRAPPER_GP(X, GPFIT);

%central differences
%     Jmat(:,i)   = (fp - fm)'/deltas(i)*0.5;
%backward difference
%     Jmat(:,i)   = (f0 - fm)'/deltas(i);
%forward difference
    Jmat(:,i)   = (fp - f0)'/deltas(i);
end

%==========================================================================
% end of operations
%==========================================================================

return