%==========================================================================
% input values: what quantities to plot and what speed to plot at 
%==========================================================================

icol            = 6;        % 1,2: c,w tilt; 3,4: c,w RPMs; 5: th0; 6: elevator

%==========================================================================
% 45 m/s: check pitching moment
%==========================================================================

V               = 45;
irow            = [5];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);

%==========================================================================
% 50 m/s: check pitching moment, thrust of R5,R7, torque of R 1,2,4,5,8
%==========================================================================

V               = 50;
irow            = [5 11 13 15 16 18 19 22];      % 1-6: body loads; 7 - 14: thrust; 15 - 22: torques
[x,y1,y2]       = load_and_project(V, p1, p2, irow, icol);


disp('completed perturbation studies for elevator deflections')